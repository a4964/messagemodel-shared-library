# MessageModel - Shared Library

Libreria utilizzata per la gestione del modello nella comunicazione tramite RabbitMQ. La libreria è integrata nei due microservizi, ServiceApi e ServiceSatellite).

## Pubblicazione su Nuget repository

La libreria è stata pacchetizzata e il pacchetto è stato pubblicato su nuget.org.

La build del pacchetto è possibile eseguirla tramite Visual Studio.

Il pacchetto è stato pushato sul repository tramite il comando:

```
dotnet nuget push MessageModel.{PACKAGE_VERSION}.nupkg --api-key {PRIVATE_API_KEY} --source https://api.nuget.org/v3/index.json

```

E' possibile trovarlo al seguente link:

https://www.nuget.org/packages/MessageModel/
