﻿using System;

namespace MessageModel
{
    public class ReferenceQueue
    {
        public int IdReference { get; set; }
        public int IdMessage { get; set; }
        public DateTime ReferenceDateTime { get; set; }
    }
}
