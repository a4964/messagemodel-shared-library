﻿namespace MessageModel
{
    public class MessageQueue
    {
        public int IdMessage { get; set; }
        public int Value { get; set; }
    }
}
